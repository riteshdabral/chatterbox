/**
 * require http and socket
 */

 const http         = require('http');
 const socketBack   = require('socket.io');
 const socketClient = require('socket.io-client');

 let httpServer;
 let ioServer;
 let httpServerAdd;
 let socket;

 /**
  * Set up http and ws 
  */
 beforeAll((done)=>{
    httpServer = http.createServer();
    httpServerAdd = httpServer.listen().address();
    ioServer = socketBack(httpServer);
    done();
 })


 beforeEach((done) => {
    // Setup
    // Do not hardcode server port and address, square brackets are used for IPv6
    socket = socketClient.connect(`http://[${httpServerAdd.address}]:${httpServerAdd.port}`, {
      'reconnection delay': 0,
      'reopen delay': 0,
      'force new connection': true,
      transports: ['websocket'],
    });
    socket.on('connect', () => {
      done();
    });
  });


 /**
  * Main Tests
  * 
  */
 describe('SOCKET TESTING', ()=>{

    /* New Socket Connection */
     test('Should Setup New Connection', (done)=>{
        ioServer.on('connection', (miniSocket)=>{

           miniSocket.on('sendMsg', data=>{
              miniSocket.emit('clientReqReceived', `This is sent back to client`);
           })

            expect(miniSocket).toBeDefined(); 
        })

        done();
     })

    /* Emit an event */

    test('Should emit from server to client', (done)=>{

        ioServer.emit('sendMsg', 'Working Fine');

        socket.once('sendMsg', (message) => {
          // Check that the message matches
          expect(message).toBe('Working Fine');
          done();
        });

    });


        /* Emit an event */

    test('Should emit from client to server', (done)=>{
      
      // send msg from client
      socket.emit('sendMsg', 'Testing msg from client');
      
      // after successful handshake within 5000ms , ioSocket should emit
      socket.on('clientReqReceived',data=>{
         expect(data).not.toBeNull();
         expect(data).toBe('This is sent back to client');
      })

      setTimeout(()=>{
         done();
      },50);
    });

 })


 afterEach((done)=>{
    if(socket.connected){
        socket.disconnect();
    }
    done();
 })


 /**
  * Clean up
  * 
  */
 afterAll((done)=>{
    ioServer.close();
    httpServer.close();
    done();
 })

 
