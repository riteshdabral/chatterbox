
/**
 * Close the person list section
 * 
 */
function closePersonListContainer(){

    document.getElementById('close-person-list-section').style.display = "none";
    

    const person_list_section = document.getElementById('person-list-section');

    person_list_section.classList.replace('open-container','close-container');

    setTimeout(()=>{
        person_list_section.style.display = "none";
        document.getElementById('open-person-list-section').style.display  = "block";
    },400);
}

/**
 * Opens the person list section
 */
function openPersonListContainer(){

    document.getElementById('open-person-list-section').style.display = "none";

    const person_list_section = document.getElementById('person-list-section');

    person_list_section.classList.replace('close-container','open-container');

    person_list_section.style.display = "block";
    
    document.getElementById('close-person-list-section').style.display = "block";

}

/**
 * changes the room on clicking button
 */
function changeroom(roomname){
    const roomchat = document.getElementById('room-chat');
    const privatechat = document.getElementById('private-chat');

    if(roomname==='room-chat' || roomname==='' || !roomname){
        privatechat.style.display="none";
        roomchat.style.display="block";
        document.getElementById('selected-private-id').value = "";
        closePersonListContainer();
    }
    else if(roomname==='private-chat'){
        privatechat.style.display="block";
        roomchat.style.display="none";
    }

    const selectedIDValue = document.getElementById('selected-private-id').value;
    document.getElementById('active-window').innerText = (roomname==='room-chat')?('ChatterBox Main'):(`Private : ${selectedIDValue}`);
}

function setChatWindowMargin(){
    var width = window.innerWidth;
    
    let minvw = width*0.31;
    let rightMargin = (360<=minvw)? (360) : (minvw);
    
    document.getElementById('room-chat').style.marginRight = `${rightMargin}px`;
    document.getElementById('active-window').style.marginRight = `${rightMargin}px`;
    document.getElementById('private-chat').style.marginRight = `${rightMargin}px`;
}