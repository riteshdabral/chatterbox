
// When window loads completely
window.onload = function(){

    setChatWindowMargin();
    // used as hash map
    const allUID = {};

    // checks for existence of alias
    checkAliasExistence();

    // sets the alias of user
    setAlias();


    var socket = io.connect();

    //var for Client
    var message  = document.getElementById('message'),
        output   = document.getElementById('room-chat'),
        alias    = document.getElementById('alias'),
        sendBtn  = document.getElementById('send'),
        feedback = document.getElementById('feedback');

    
    // Send Button Event
    sendBtn.addEventListener('click',()=>{
        const selectedId = document.getElementById('selected-private-id').value;

        if(message.value!=="" && selectedId===''){
            socket.emit('chat',{
                message : message.value,
                alias : alias.value
            });

            message.value="";
        }
        else if(message.value!=="" && selectedId && allUID[`${selectedId}`]){
            
            //  add this msg to your div
            const privateChatDiv = document.getElementById(`${selectedId}-user-div`);

            if(privateChatDiv){

                privateChatDiv.innerHTML+=`<div class="output-right"><div class="text-chat-container"><h3 class="textAliasHeader">You</h3><p>${message.value}</p></div></div>`;

                socket.emit('private-chat',{
                    message : message.value,
                    alias : alias.value,
                    to:selectedId
                });
            }

            message.value = "";
        }
    })


    // typing
    message.addEventListener('keypress',()=>{
        socket.emit('typing',alias.value);
    })


    // Allows Room Chat
    socket.on('chat',socketData=>{
        const {data,uid} = socketData;

        if(!allUID[`${uid}`] && uid!==socket.id){
            allUID[`${uid}`] = uid;
            
            setUserList(uid,data.alias);
        }

        feedback.innerHTML="";

        let divClass = 'output-left';

        if(uid===socket.id){
            divClass = 'output-right'
        }
        output.innerHTML+=`<div class=${divClass}><div class="text-chat-container"><h3 class="textAliasHeader">${data.alias}<span>(${uid})</span></h3><p>${data.message}</p></div></div>`;
    });

    socket.on('typing',(data)=>{
        feedback.innerHTML=`<p>${data} is typing...`;
    })


    // allows private chat 
    socket.on('private-chat', (data)=>{
        const {alias,to,from} = data;

        let thisOne = (to!==socket.id)?(to):(from);

        let privateChatDiv = document.getElementById(`${thisOne}-user-div`);

        if(!privateChatDiv){
            allUID[`${thisOne}`] = thisOne;
            setUserList(thisOne,alias);
            privateChatDiv = document.getElementById(`${thisOne}-user-div`);        
        }

        privateChatDiv.innerHTML+=`<div class="output-left"><div class="text-chat-container"><h3 class="textAliasHeader">${alias}<span>(${thisOne})</span></h3><p>${data.message}</p></div></div>`;

    })


    // On socket disconnet
    socket.on('user_disconnected', (data)=>{
        const {uid} = data; 
        resetUserList(uid);
    })
}


/**
 * Fix window resizer
 */
window.addEventListener('resize', ()=>{
    setChatWindowMargin();
})
/**
 * 
 * Remove user from user list
 *  
 * @param {*} uid : socket id
 */

function resetUserList(uid){
    const userLists = document.getElementById('user-lists');
    const element = document.getElementById(`${uid}`);

    /* remove a div in private-chat as well */
    const privateChatDiv = document.getElementById(`${uid}-user-div`);

    if(element){
        element.remove();
    }
    if(privateChatDiv){
        privateChatDiv.remove();
    }

    if(!userLists.hasChildNodes()){
        userLists.style.display = "none";
    }

}


/**
 * add user to list
 * 
 * @param {*} uid : socket id
 * @param {*} alias : alias
 */
function setUserList(uid,alias){
    const userLists = document.getElementById('user-lists');
    if(!userLists.hasChildNodes()){
        userLists.style.display = "block";
    }
    userLists.innerHTML+=`<h6 onclick="startPrivateChat('${uid}','${alias}')" id="${uid}">${alias} (<span style="font-size:12px">${uid}</span>) </h6>`;

    /* set a div in private-chat as well */
    const privateChat = document.getElementById('private-chat');
    privateChat.innerHTML+=`<div style="display:none" id="${uid}-user-div"></div>`;
}


/**
 * Sets the alias of user
 */

function setAlias(){
    var name = sessionStorage.getItem('alias');
    document.getElementById('alias').value = name;
}


/**
 * 
 * checks the session storage for existing alias
 * 
 */
function checkAliasExistence(){
    var name = sessionStorage.getItem('alias');
    if( !name || name==='' || !name.match(/^[a-zA-Z\s]+$/)){
        sessionStorage.clear();
        document.getElementById('container').style.display = "block";
        return;
    }

    document.getElementById('container').style.display = "none";
}


/**
 * Validate the user name upon landing
 * 
 */
function validateAlias(){
    var aliasInput = document.getElementById('aliasInput').value.trim();
    var aliasInputAlert =  document.getElementById('aliasInputAlert'); 

    aliasInputAlert.style.display = "none";

    if(aliasInput==='' || !aliasInput.match(/^[a-zA-Z\s]+$/)){
        aliasInputAlert.style.display = "block";
        aliasInputAlert.innerText = `Invalid alias (Alphabets only)`;
       return;
    }
 
    sessionStorage.setItem('alias', aliasInput);
    
    document.getElementById('container').style.display = "none";

    document.getElementById('chat').style.filter = "none";
    setAlias();
}



/**
 * Start Private Chat,
 * 
 * @param {*} uid : socket id
 */
function startPrivateChat(uid,alias){
    changeroom('private-chat');
    closePersonListContainer();

    const selectedID = document.getElementById('selected-private-id');
    if(selectedID.value!==''){
        const alreadyOpen = document.getElementById(`${selectedID.value}-user-div`);
        if(alreadyOpen){
            alreadyOpen.style.display="none";
        }
    }

    document.getElementById(`${uid}-user-div`).style.display = "block";
    selectedID.value = uid;

    document.getElementById('active-window').innerText = `${alias} : (${uid})`;
}