# ***`Chatterbox`***[](https://chatterbokx.herokuapp.com)
Real time chatting application based on NodeJS and WS (Socket.io) connections which works on session only and currently does not support any database for storing the chats

## Requirements
  * NodeJS (curr ver - 12)
  * Browser

## Scripts
The project consists of well written and easily understandable test cases and descriptions.
#### `npm start`
     - This starts the project in development mode
#### `npm test`
     - This tests the socket functionings

## Steps
1. run command `git clone https://gitlab.com/riteshdabral/chatterbox.git`
2. Within the root directory, run command `npm install`
3. Run the tests to make sure everything is working fine
4. If all test cases passed, run command `npm start` from root directory
5. Open your browser and check the application

## Live application
https://chatterbokx.herokuapp.com
