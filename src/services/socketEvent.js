/**
 * io : Socket Configuration
 * 
 */
 
const io = require('../loaders/express');

 io.on('connection', (socket)=>{
    
    console.log(`${socket.id} connected`);
    
    let uid = socket.id;

    socket.on('chat', (data)=>{
        io.sockets.emit('chat',{data,uid});
    })

    socket.on('typing',(data)=>{
        socket.broadcast.emit('typing',{data,uid});
    })

    socket.on("disconnect", (reason) => {
       console.log(`${socket.id} disconnected`);
       socket.broadcast.emit('user_disconnected', {uid:socket.id})
    });

    socket.on('private-chat', (data)=>{
        io.to(data['to']).emit("private-chat", {
            message:data.message,
            alias:data.alias,
            to:data.to,
            from:socket.id
        });
    })

 })


module.exports = io;