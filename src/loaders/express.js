/**
 * express : HTTP calls
 * socket : socket calls 
 */
 const express = require('express');
 const socket  = require('socket.io');

/**
 * PORT : application's port no.
 * app : object of type express.Application
 * server 
 * io : socket configuration
 * 
 */

 const app    = express();
 const PORT   = process.env.PORT || 5000;
 const server = app.listen(PORT, ()=>{
     console.log(`S E R V E R   @   ${PORT}`);
 });

 const io = socket(server);

 console.log('X--------- EXPRESS LOADED ---------X');

 app.use(express.static('./public'));

 app.get('/', (req,res)=>{
     res.send(`<center><h1>CHATTER BOX</h1></center>`)
 })
     
 /**  
 * 404  E R R O R   H A N D L I N G
 */
 app.use((req, res, next) => {
    const err = new Error('Not Found');
    err['status'] = 404;
    next(err);
 });

/**
 * A L L    E R R O R   H A N D L I N G
 */
 app.use((err,req,res,next)=>{
    res.status(err.status || 500);
    res.json({
        errors:{
            message: err.message
        }
    })
 });

/*------- MODULE EXPORTS ------------*/
 module.exports = io;
